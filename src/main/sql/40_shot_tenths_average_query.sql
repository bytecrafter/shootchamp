SELECT results.date as time, (res_in_tenths, series.count), ROUND(results.res_in_tenths / series.count) * 40 AS value
FROM results,
     (SELECT results.id, SUM(shots.count) AS count
      FROM results,
           (SELECT series.result_id, COUNT(shots.*) AS count
            FROM shots,
                 series
            WHERE shots.series_id = series.id
            GROUP BY series.id) AS shots
      WHERE shots.result_id = results.id
      GROUP BY results.id) AS series
WHERE results.id = series.id;