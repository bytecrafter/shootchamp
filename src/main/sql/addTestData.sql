INSERT INTO results (weapon_type, date, result, res_in_tenths, avgx, avgy, spread, spread_h, spread_v) VALUES ('LG', '2023-06-13', 180, 182.2, 2.3, -3.2, 3.3, 0, 10.2);
INSERT INTO series (result_id, s_res, s_res_in_tenths, avgx, avgy, spread, spread_h, spread_v) VALUES ((SELECT id FROM results ORDER BY id DESC LIMIT 1), 100, 101.1, 2.3, -3.2, 3.3, 0, 10.2);
INSERT INTO shots (series_id, value) VALUES ((SELECT id FROM series ORDER BY id DESC LIMIT 1), 10.9);
INSERT INTO shots (series_id, value) VALUES ((SELECT id FROM series ORDER BY id DESC LIMIT 1), 5.9);
INSERT INTO series (result_id, s_res, s_res_in_tenths, avgx, avgy, spread, spread_h, spread_v) VALUES ((SELECT id FROM results ORDER BY id DESC LIMIT 1), 80, 81.1, 2.3, -3.2, 3.3, 0, 10.2);
INSERT INTO shots (series_id, value) VALUES ((SELECT id FROM series ORDER BY id DESC LIMIT 1), 10.9);
INSERT INTO shots (series_id, value) VALUES ((SELECT id FROM series ORDER BY id DESC LIMIT 1), 5.9);



INSERT INTO results (weapon_type, date, result, res_in_tenths, avgx, avgy, spread, spread_h, spread_v) VALUES ('KK', '2021-06-13', 90, 92.2, 0, 3.2, 3.3, 0, 10.2);
INSERT INTO series (result_id, s_res, s_res_in_tenths, avgx, avgy, spread, spread_h, spread_v) VALUES ((SELECT id FROM results ORDER BY id DESC LIMIT 1), 50, 51.1, 2.3, -3.2, 3.3, 0, 10.2);
INSERT INTO shots (series_id, value) VALUES ((SELECT id FROM series ORDER BY id DESC LIMIT 1), 10.9);
INSERT INTO shots (series_id, value) VALUES ((SELECT id FROM series ORDER BY id DESC LIMIT 1), 5.9);
INSERT INTO series (result_id, s_res, s_res_in_tenths, avgx, avgy, spread, spread_h, spread_v) VALUES ((SELECT id FROM results ORDER BY id DESC LIMIT 1), 40, 41.1, 2.3, -3.2, 3.3, 0, 10.2);
INSERT INTO shots (series_id, value) VALUES ((SELECT id FROM series ORDER BY id DESC LIMIT 1), 10.9);
INSERT INTO shots (series_id, value) VALUES ((SELECT id FROM series ORDER BY id DESC LIMIT 1), 5.9);