DROP TABLE IF EXISTS shots;
DROP TABLE IF EXISTS series;
DROP TABLE IF EXISTS results;
DROP TYPE IF EXISTS weapon_type;

CREATE TYPE weapon_type AS ENUM ('LG', 'LP', 'KK');
CREATE TABLE results (
    id              SERIAL      PRIMARY KEY,
    weapon_type     weapon_type NOT NULL DEFAULT 'LG',
    date            date        NOT NULL,
    result          integer     NOT NULL,
    res_in_tenths   real        NOT NULL,
    avgX            real        NOT NULL,
    avgY            real        NOT NULL,
    spread          real        NOT NULL,
    spread_h        real        NOT NULL,
    spread_v        real        NOT NULL
);

CREATE TABLE series (
    id              SERIAL      PRIMARY KEY,
    result_id       integer     NOT NULL        REFERENCES results (id) ON DELETE CASCADE,
    s_res           integer     NOT NULL,
    s_res_in_tenths real        NOT NULL,
    avgX            real        NOT NULL,
    avgY            real        NOT NULL,
    spread          real        NOT NULL,
    spread_h        real        NOT NULL,
    spread_v        real        NOT NULL
);

CREATE TABLE shots (
    id          SERIAL      PRIMARY KEY,
    series_id   integer     NOT NULL        REFERENCES series (id) ON DELETE CASCADE,
    value       real        NOT NULL
);

GRANT SELECT ON ALL TABLES IN SCHEMA public TO "ShootChamp";
GRANT INSERT ON ALL TABLES IN SCHEMA public TO "ShootChamp";
GRANT UPDATE ON ALL SEQUENCES IN SCHEMA public TO "ShootChamp";