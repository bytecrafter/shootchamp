package io;

import shootingresults.Result;
import shootingresults.Series;

import java.sql.*;

public class DatabaseHandler {

    private static final String URL = "jdbc:postgresql://localhost:5432/ShootChamp";
    private static final String USER = "ShootChamp";
    private static final String PASSWORD = Credentials.getPassword();

    public static void putResIntoDB(Result result) {

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {

            PreparedStatement statement = connection.prepareStatement("INSERT INTO results (" + /*weapon_type, */"date, result, res_in_tenths, avgx, avgy, spread, spread_h, spread_v)  VALUES (" + /*"'" + result.getTypeString() + "', */"?, ?, ?, ?, ?, ?, ?, ?)");
            //statement.setString(1, result.getTypeString());
            statement.setDate(1, result.getDate());
            statement.setInt(2, result.getRes());
            statement.setDouble(3, result.getResInTenth() / 10.0);
            statement.setDouble(4, result.getAveragePosition()[0]);
            statement.setDouble(5, result.getAveragePosition()[1]);
            statement.setDouble(6, result.getSpread()[0]);
            statement.setDouble(7, result.getSpread()[1]);
            statement.setDouble(8, result.getSpread()[2]);

            statement.execute();
            statement.close();

            ResultSet lastResultID = connection.createStatement().executeQuery("SELECT id FROM results ORDER BY id DESC LIMIT 1");
            lastResultID.next();
            int resultID = lastResultID.getInt(1);

            for (Series series : result.getSeries()) {

                statement = connection.prepareStatement("INSERT INTO series (result_id, s_res, s_res_in_tenths, avgx, avgy, spread, spread_h, spread_v) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                statement.setInt(1, resultID);
                statement.setInt(2, series.getRes());
                statement.setDouble(3, series.getResInTenths() / 10.0);
                statement.setDouble(4, series.getAveragePostition()[0]);
                statement.setDouble(5, series.getAveragePostition()[1]);
                statement.setDouble(6, series.getSpread()[0]);
                statement.setDouble(7, series.getSpread()[1]);
                statement.setDouble(8, series.getSpread()[2]);

                statement.execute();
                statement.close();

                ResultSet lastSeriesID = connection.createStatement().executeQuery("SELECT id FROM series ORDER BY id DESC LIMIT 1");
                lastSeriesID.next();
                int seriesID = lastSeriesID.getInt(1);

                for (int shot : series.getShots()) {

                    statement = connection.prepareStatement("INSERT INTO shots (series_id, value) VALUES (?, ?)");
                    statement.setInt(1, seriesID);
                    statement.setDouble(2, shot / 10.0);

                    statement.execute();
                    statement.close();

                }

            }

        } catch (SQLException e) {
            System.out.printf("Result from %s was not able to be inserted into the database:%n", result.getDate().toString());
            e.printStackTrace();
        }

    }


}
