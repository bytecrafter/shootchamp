package io;

import nu.pattern.OpenCV;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Input {

    public static List<Mat> getImageFile(File file) {

        OpenCV.loadLocally();

        List<Mat> images;

        if (file.getName().endsWith(".pdf")) {
            images = getConvertedImages(file);
        } else {
            images = new ArrayList<>();
            images.add(Imgcodecs.imread(file.getAbsolutePath(), Imgcodecs.IMREAD_COLOR));
        }

        return images;

    }

    private static List<Mat> getConvertedImages(File file) {

        List<Mat> images = new ArrayList<>();

        // TODO maybe speed this up lol (if pawbssible)
        try (PDDocument pdf = PDDocument.load(file)) {
            PDFRenderer render = new PDFRenderer(pdf);
            for (int page = 0; page < pdf.getNumberOfPages(); page++) {
                BufferedImage renderedPage = render.renderImageWithDPI(page, 500, ImageType.BGR);
                byte[] imagePixels = ((DataBufferByte) renderedPage.getRaster().getDataBuffer()).getData();
                Mat image = new Mat(renderedPage.getHeight(), renderedPage.getWidth(), CvType.CV_8UC3);
                image.put(0, 0, imagePixels);
                images.add(image);
            }
        } catch (IOException e) {
            System.out.println("Unable to load file: ");
            e.printStackTrace();
        }

        return images;

    }

}
