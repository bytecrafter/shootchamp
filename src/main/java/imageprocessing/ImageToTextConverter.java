package imageprocessing;

import io.Credentials;
import org.bytedeco.leptonica.PIX;
import org.bytedeco.leptonica.global.leptonica;
import org.bytedeco.tesseract.TessBaseAPI;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import java.nio.ByteBuffer;

import static org.bytedeco.tesseract.global.tesseract.PSM_AUTO_OSD;

public class ImageToTextConverter {

    public static String convert(Mat in) {

        try (TessBaseAPI baseAPI = new TessBaseAPI()) {

            if (baseAPI.Init(Credentials.getTesspath(), "deu", 0) != 0) { // (0 -> engine option 0) TODO make this as users's choice and doin't have it static (or dl it if not exist)
                System.err.println("Tesseract init failed.");
                return null;
            }

            PIX input = new PIX(convertMatToPix(in)); // do I still have to deallocate these? or is it fine? This is fiiine
            baseAPI.SetImage(input);
            baseAPI.SetPageSegMode(PSM_AUTO_OSD);
            return baseAPI.GetUTF8Text().getString();
        }
    }


    // haha stackoverflow go brrr (might want to link for later heh https://stackoverflow.com/questions/48868503/leptonica-opencv-java-convert-mat-to-pix-and-vise-versa)
    private static PIX convertMatToPix(Mat mat) {
        MatOfByte bytes = new MatOfByte();
        Imgcodecs.imencode(".jpg", mat, bytes);
        ByteBuffer buff = ByteBuffer.wrap(bytes.toArray());
        return leptonica.pixReadMem(buff, buff.capacity());
    }

}
