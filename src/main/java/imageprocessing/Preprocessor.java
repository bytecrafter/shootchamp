package imageprocessing;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class Preprocessor {

    private static final byte FILTER_SIZE = 5; //TODO maybe play wit dese sum more, depending on if needed or not
    private static final byte THRESHOLD = 100;

    public static List<Mat> processImages(List<Mat> images) {

        List<Mat> processedImages = new ArrayList<>();

        for (Mat image : images) {
            image = grayscale(image);
            image = binarize(image);
            image = blur(image);
            processedImages.add(image);
        }

        return processedImages;

    }


    private static Mat grayscale(Mat in) {
        Mat out = new Mat();
        Imgproc.cvtColor(in, out, Imgproc.COLOR_BGR2GRAY);
        return out;
    }

    private static Mat blur(Mat in) {
        Mat out = new Mat();
        Imgproc.GaussianBlur(in, out, new Size(FILTER_SIZE, FILTER_SIZE), 0, 0);
        return out;
    }

    private static Mat binarize(Mat in) {
        Mat out = new Mat();
        Imgproc.threshold(in, out, THRESHOLD, 255, 0);
        return out;
    }

}
