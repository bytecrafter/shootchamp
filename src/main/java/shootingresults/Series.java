package shootingresults;

import java.util.Arrays;

public class Series {

    private final int[] shots; // saved multiplied by 10 to avoid floats
    private final double[] averagePostition;
    private final double[] spread;

    public Series(int[] shots, double[] averagePostition, double[] spread) {
        this.shots = shots;
        this.averagePostition = averagePostition;
        this.spread = spread;
    }

    public int getRes() {
        int res = 0;
        for (int s : shots) {
            res += s / 10; // due to it being multiplied by 10 for saving in int
        }
        return res;
    }

    public int getResInTenths() {
        int res = 0;
        for (int s : shots) {
            res += s;
        }
        return res;
    }

    public int[] getShots() {
        return shots;
    }

    public double[] getAveragePostition() {
        return averagePostition;
    }

    public double[] getSpread() {
        return spread;
    }

    @Override
    public String toString() {
        return "%n%s%n%s%n%s%n".formatted(Arrays.toString(shots), Arrays.toString(averagePostition), Arrays.toString(spread));
    }

}
