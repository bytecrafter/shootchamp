package shootingresults;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Result {

//    private final Type type;
    private final Date date;
    private final int res;
    private final int resInTenth; // saved multiplied by 10 to avoid floats
    private final double[] averagePosition;
    private final double[] spread;
    private final List<Series> series;

    public Result(/*Type type, */Date date, int res, int resInTenth, double[] averagePosition, double[] spread) {
//        this.type = type;
        this.date = date;
        this.res = res;
        this.resInTenth = resInTenth;
        this.averagePosition = averagePosition;
        this.spread = spread;
        this.series = new ArrayList<>();
    }

    public void addSeries(Series s) {
        series.add(s);
    }

    public boolean isLogicc() {

        int testRes = 0;
        int testResInTenths = 0;
        for (Series s : series) {
            testRes += s.getRes();
            testResInTenths += s.getResInTenths();
        }
        if (testRes != res) return false;
        return testResInTenths == resInTenth;

    }

    @Override
    public String toString() {
        if (series.isEmpty())
            return "Date: %s%nRes: %d (%d)%n%s%n%s".formatted(date.toString(), res, resInTenth, Arrays.toString(averagePosition), Arrays.toString(spread));
        return "Date: %s%nRes: %d (%d)%n%s%n%s%n%s".formatted(date.toString(), res, resInTenth, Arrays.toString(averagePosition), Arrays.toString(spread), series.toString());
    }

/*    public String getTypeString() {
        return type.toString();
    }
*/

    public Date getDate() {
        return date;
    }

    public int getRes() {
        return res;
    }

    public int getResInTenth() {
        return resInTenth;
    }

    public double[] getAveragePosition() {
        return averagePosition;
    }

    public double[] getSpread() {
        return spread;
    }

    public List<Series> getSeries() {
        return series;
    }

}

/*enum Type {
    LG,
    LP,
    KK
}
*/