package shootingresults;

import java.sql.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResultCreator {

    private static final String[] months = {"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"}; // WTF AAAAHHHHHHHH do I really?!

    public static Result textToResult(String in) {

        Scanner scanner = new Scanner(System.in);

//        Type type;
        Date date;
        int res = -1;
        int resInTenths = -1;
        double[] averagePosition;
        double[] spread;

        String[] lines = in.split("\n");
        int[] line = {0, 0}; // Saved in an array so it can be passed to a function by reference to keep the changes (so that the line doesn't start from the beginning every time but goes through the doc), second value is a "snapshot" made before the function changed it

/*        type = switch (lines[line[0]].substring(0, 2)) {
            case "LG" -> Type.LG;
            case "LP" -> Type.LP;
            case "KK" -> Type.KK;
            default -> null;
        };
*/

        date = getDate(lines, line);

        // trust the user that they ain't too incompetent O_o
        if (date == null) {
            System.out.println("The parser was not able to find the date, please input manually in the format dd:mm:yyyy:");
            String input = scanner.nextLine();
            String[] inputsplit = input.split(":");
            date = new Date(Integer.parseInt(inputsplit[2]) - 1900, Integer.parseInt(inputsplit[1]) - 1, Integer.parseInt(inputsplit[0]));
            line[0] = line[1]; // reverting of snapshot cuz date not found also means that it arrived at end of file so line has to be reset
        }
        line[1] = line[0];


        for (; line[0] < lines.length; line[0]++) {
            if (lines[line[0]].contains("Erge") || lines[line[0]].contains("bnis")) {
                Matcher resMatch = Pattern.compile("(?<!\\.)\\d+(?![.\\d])").matcher(lines[line[0]]); // RegEx fun YYAAAAAYYYYY
                if (resMatch.find()) res = Integer.parseInt(resMatch.group(0));
                else {
                    System.out.println("The parser was not able to recognize the result, please input manually:");
                    String input = scanner.nextLine();
                    res = Integer.parseInt(input);
                }
                Matcher tenthsMatch = Pattern.compile("\\d+\\.\\d").matcher(lines[line[0]]);
                if (tenthsMatch.find()) resInTenths = Integer.parseInt(tenthsMatch.group(0).replace(".", ""));
                else {
                    System.out.println("The parser was not able to recognize the result in tenths, please input manually in format xxx.x:");
                    String input = scanner.nextLine();
                    input = input.replace(".", "");
                    resInTenths = Integer.parseInt(input);
                }
                break;
            }
        }

        if (res == -1 || resInTenths == -1) {
            System.out.println("The parser was not able to find the results, please input them manually.");
            System.out.println("First, please put in the normal result:");
            String input = scanner.nextLine();
            res = Integer.parseInt(input);
            System.out.println("Now please input the result in tenths in the format xxx.x:");
            input = scanner.nextLine();
            input = input.replace(".", "");
            resInTenths = Integer.parseInt(input);
            line[0] = line[1]; // reverting of snapshot cuz date not found also means that it arrived at end of file so line has to be reset
        }
        line[1] = line[0];

        averagePosition = getAveragePosition(lines, line);

        if (averagePosition == null) {
            System.out.println("The parser was not able to correctly identify the total average position, please input it manually.");
            System.out.println("First, put in the horizontal position in the format x.xxl or x.xxr for left or right:");
            String input = scanner.nextLine();
            double xAxis = Double.parseDouble(input.substring(0, 4));
            if (input.endsWith("l")) xAxis *= -1;

            System.out.println("Now, put in the vertical position in the format x.xxh or x.xxl for high or low:");
            input = scanner.nextLine();
            double yAxis = Double.parseDouble(input.substring(0, 4));
            if (input.endsWith("l")) yAxis *= -1;

            averagePosition = new double[]{xAxis, yAxis};
            line[0] = line[1]; // reverting of snapshot cuz date not found also means that it arrived at end of file so line has to be reset
        }
        line[1] = line[0];

        spread = getSpread(lines, line);

        if (spread == null) {
            System.out.println("The parser was not able to correctly identify the total spread, please input it manually.");
            System.out.println("First, put in the total spread in the format x.xx or x.xx:");
            String input = scanner.nextLine();
            double total = Double.parseDouble(input.substring(0, 4));

            System.out.println("Then, put in the horizontal spread in the format x.xx:");
            input = scanner.nextLine();
            double horizontal = Double.parseDouble(input.substring(0, 4));

            System.out.println("Last, put in the vertical spread in the format x.xx:");
            input = scanner.nextLine();
            double vertical = Double.parseDouble(input.substring(0, 4));

            spread = new double[]{total, horizontal, vertical};
            line[0] = line[1]; // reverting of snapshot cuz date not found also means that it arrived at end of file so line has to be reset
        }
        line[1] = line[0];

        Result result = new Result(/*type,*/ date, res, resInTenths, averagePosition, spread);

        addSeries(result, lines, line);

        if(!result.isLogicc()) {
            System.out.println("Something went wrong when parsing the result and rn I cannot be bothered to check y, maybe u want, entering stuffs manually might come soon:TM:");
            return null;
        }

        return result;

    }

    /**
     * Searches doc until it found the regex that (usually -.-) indicates the date
     **/
    private static Date getDate(String[] lines, int[] line) {
        Pattern datePattern = Pattern.compile("((0?\\d|[12]\\d|3[01])\\.(0?\\d|1[0-2])\\.\\d+)"); // Regex describing how the date usually looks
        Pattern timePattern = Pattern.compile("(([01]\\d|2[0-3]):([0-5]\\d))"); // Regex describing how the time looks if the datecheck fails
        for (; line[0] < lines.length; line[0]++) {

            Matcher dateMatcher = datePattern.matcher(lines[line[0]]);
            Matcher timeMatcher = timePattern.matcher(lines[line[0]]);

            if (dateMatcher.find()) {
                String[] dateString = dateMatcher.group(0).split("\\.");
                return new Date(Integer.parseInt(dateString[2]) - 1900, Integer.parseInt(dateString[1]) - 1, Integer.parseInt(dateString[0]));
            } else {
                int month = containsMonth(lines[line[0]]);
                if (month != -1 && timeMatcher.find()) {
                    String[] splitline = lines[line[0]].split(months[month]);
                    // accounting for possible differences of scan (space / no space etc)
                    splitline[0] = splitline[0].replace(" ", "").replace(".", "");
                    splitline[1] = splitline[1].replace(" ", "").replace(".", "");
                    return new Date(Integer.parseInt(splitline[1].substring(0, 4)) - 1900, month, Integer.parseInt(splitline[0].substring(splitline[0].length() - 2)));
                }
            }
        }
        return null;
    }

    /**
     * Searches doc until it finds a line which should include "Trefferlage", indicatign the average position
     **/
    private static double[] getAveragePosition(String[] lines, int[] line) {
        for (; line[0] < lines.length; line[0]++) {

            if (lines[line[0]].contains("Serie") && !lines[line[0]].contains("Serien"))
                return null; // -> went too far without finding (yes this relies on recognizing "Serie", not ideal)

            if (lines[line[0]].contains("Treffer") || lines[line[0]].contains("lage")) {

                Matcher matcher = Pattern.compile("\\d+\\.\\d+").matcher(lines[line[0]]); // Regex for easier extraction of numbers

                if (!matcher.find()) return null;
                double xAxis = Double.parseDouble(matcher.group());

                if (!matcher.find()) return null;
                double yAxis = Double.parseDouble(matcher.group());

                if (lines[line[0]].contains("links")) xAxis *= -1;
                else if (!lines[line[0]].contains("rechts") && xAxis != 0.0)
                    return null; // -> this means that the direction was not able to be parsed correctly, thus messing up the data

                if (lines[line[0]].contains("tief")) yAxis *= -1;
                else if (!lines[line[0]].contains("hoch") && yAxis != 0.0) return null; // -> same here

                return new double[]{xAxis, yAxis};
            }
        }
        return null;
    }

    /**
     * Searches doc until it finds a line which should include "Streuwert", indicating the spread
     **/
    private static double[] getSpread(String[] lines, int[] line) {
        for (; line[0] < lines.length; line[0]++) {

            if (lines[line[0]].contains("serie") && !lines[line[0]].contains("Serien"))
                return null; // -> went too far without finding

            if (lines[line[0]].contains("Streu") || lines[line[0]].contains("wert")) {

                Matcher matcher = Pattern.compile("\\d+\\.\\d+").matcher(lines[line[0]]); // Regex for easier extraction of numbers

                if (!matcher.find()) return null;
                double total = Double.parseDouble(matcher.group());

                if (!matcher.find()) return null;
                double horizontal = Double.parseDouble(matcher.group());

                if (!matcher.find()) return null;
                double vertical = Double.parseDouble(matcher.group());

                return new double[]{total, horizontal, vertical};
            }
        }
        return null;
    }

    /**
     * Iterates over the rest of the doc searching for series and then adding those to the result
     **/
    private static void addSeries(Result result, String[] lines, int[] line) {

        for (; line[0] < lines.length; line[0]++) {

            Pattern shotPattern = Pattern.compile("(\\d+|1[oO])\\S\\d"); // Regex describing how a sicngle shot *could* look like

            if (lines[line[0]].contains("Ser") || lines[line[0]].contains("rie")) {
                line[0]++;
                int[] shots = new int[10];

                boolean isDone = false;
                Matcher matcher = shotPattern.matcher(lines[line[0]]);
                for (int i = 0; i < 5; i++) {
                    if (!matcher.find()) {
                        System.out.printf("U sure series ended after %d shots?%n", i); // TODO fix dis maybe some point
                        System.out.println("Nya too bad there's not really enough time anymore to implement any fixes for that, maybe later, sowwy ;-;");
                        isDone = true;
                        break;
                    }
                    String shot = matcher.group().replace("o", "0").replace("O", "0"); // in case a 0 got recognized as an o or O (cuz das easy fix)
                    shots[i] = Integer.parseInt(shot.substring(0, shot.length() - 2) + shot.substring(shot.length() - 1)); // removing the splitting part, be that the decimal point or some other character
                }
                line[0]++;

                matcher = shotPattern.matcher(lines[line[0]]);
                for (int i = 0; i < 5; i++) {
                    if (!matcher.find()) {
                        System.out.printf("U sure series ended after %d shots?%n", i + 5); // TODO fix dis maybe some point
                        System.out.println("Nya too bad there's not really enough time anymore to implement any fixes for that, maybe later, sowwy ;-;");
                        isDone = true;
                        break;
                    }
                    String shot = matcher.group().replace("o", "0").replace("O", "0");
                    shots[i + 5] = Integer.parseInt(shot.substring(0, shot.length() - 2) + shot.substring(shot.length() - 1)); // removing the splitting part, be that the decimal point or some other character
                }
                line[1] = line[0];

                double[] averageSeriesPosition = getAveragePosition(lines, line);
                if (averageSeriesPosition == null) {

                    Scanner scanner = new Scanner(System.in);

                    System.out.printf("The parser was not able to correctly identify the average position of series %d, please input it manually.%n", result.getSeries().size() + 1);
                    System.out.println("First, put in the horizontal position in the format x.xxl or x.xxr for left or right:");
                    String input = scanner.nextLine();
                    double xAxis = Double.parseDouble(input.substring(0, 4));
                    if (input.endsWith("l")) xAxis *= -1;

                    System.out.println("Now, put in the vertical position in the format x.xxh or x.xxl for high or low:");
                    input = scanner.nextLine();
                    double yAxis = Double.parseDouble(input.substring(0, 4));
                    if (input.endsWith("l")) yAxis *= -1;

                    averageSeriesPosition = new double[]{xAxis, yAxis};
                    line[0] = line[1]; // reverting of snapshot cuz date not found also means that it arrived at end of file so line has to be reset
                }
                line[1] = line[0];

                double[] seriesSpread = getSpread(lines, line);
                if (seriesSpread == null) {

                    Scanner scanner = new Scanner(System.in);

                    System.out.printf("The parser was not able to correctly identify the spread of series %d, please input it manually.%n", result.getSeries().size() + 1);
                    System.out.println("First, put in the total spread in the format x.xx or x.xx:");
                    String input = scanner.nextLine();
                    double total = Double.parseDouble(input.substring(0, 4));

                    System.out.println("Then, put in the horizontal spread in the format x.xx:");
                    input = scanner.nextLine();
                    double horizontal = Double.parseDouble(input.substring(0, 4));

                    System.out.println("Last, put in the vertical spread in the format x.xx:");
                    input = scanner.nextLine();
                    double vertical = Double.parseDouble(input.substring(0, 4));

                    seriesSpread = new double[]{total, horizontal, vertical};
                    line[0] = line[1]; // reverting of snapshot cuz date not found also means that it arrived at end of file so line has to be reset
                }
                line[1] = line[0];

                result.addSeries(new Series(shots, averageSeriesPosition, seriesSpread));

                if(isDone) return; // yee just kinda don't feel like fixing this altho it would be good, just not enought time ><

            }
        }
    }

    //** Checks if  the String contains a one of the monts **/
    private static int containsMonth(String s) {
        for (int i = 0; i < months.length; i++) {
            if (s.contains(months[i])) return i;
        }
        return -1;
    }

}
