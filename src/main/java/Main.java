import imageprocessing.ImageToTextConverter;
import imageprocessing.Preprocessor;
import io.DatabaseHandler;
import io.Input;
import org.opencv.core.Mat;
import shootingresults.Result;
import shootingresults.ResultCreator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        //List<Mat> images = Input.getImageFile(new File("testdata/Ergebnisse/22.11.2022.jpg")); // This works with some manual additions
        //List<Mat> images = Input.getImageFile(new File("testdata/Ergebnisse/Schussbild2.pdf")); // This one works for sure
        List<Mat> images = Input.getImageFile(new File("testdata/Ergebnisse/multiple.pdf"));

        images = Preprocessor.processImages(images);
        if (images.isEmpty()) System.exit(-1);

        List<Result> results = new ArrayList<>();
        for (int i = 0; i < images.size(); i++) {
            System.out.printf("Converting Page %d out of %d...%n", i + 1, images.size());
            Mat image  = images.get(i);
            String s = ImageToTextConverter.convert(image);
            if (s == null) continue;
            results.add(ResultCreator.textToResult(s));
        }

        for(Result result : results) {
            if (result == null) continue;
            DatabaseHandler.putResIntoDB(result);
        }

    }
}